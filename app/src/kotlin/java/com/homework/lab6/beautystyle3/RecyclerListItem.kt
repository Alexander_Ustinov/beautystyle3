package com.homework.lab6.beautystyle3

open class RecyclerListItem constructor(
    var name: String? = null,
    var description: String? = null,
    var imageResourceId: Int = 0
)
