package com.homework.lab6.beautystyle3.activity

enum class MainActivityExtras(private val text: String) {
    ITEM_NAME("nailName"),
    ITEM_DESCRIPTION("nailDescription"),
    ITEM_IMAGE_ID("nailImageId");

    override fun toString(): String {
        return text
    }
}