package com.homework.lab6.beautystyle3.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.homework.lab6.beautystyle3.ListItem
import com.homework.lab6.beautystyle3.R
import com.homework.lab6.beautystyle3.RecyclerListItem
import kotlinx.android.synthetic.main.item.view.*

class TopRecyclerViewAdapter(
        private val listItems: List<RecyclerListItem>,
        private val onNailClickListener: (RecyclerListItem) -> Unit
) : RecyclerView.Adapter<TopRecyclerViewAdapter.NailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NailViewHolder {
        val nailView = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return NailViewHolder(nailView)
    }

    override fun onBindViewHolder(holder: NailViewHolder, position: Int) {
        holder.bind(listItems[position])
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    inner class NailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: RecyclerListItem) {
            itemView.nail_name.text = item.name;
            itemView.nail_photo.setImageResource(item.imageResourceId)
            itemView.nail_description.text = item.description
            itemView.setOnClickListener {onNailClickListener.invoke(item)}
        }
    }
}
