package com.homework.lab6.beautystyle3.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.homework.lab6.beautystyle3.MainActivityDataHolder
import com.homework.lab6.beautystyle3.R
import com.homework.lab6.beautystyle3.RecyclerListItem
import com.homework.lab6.beautystyle3.adapter.TopRecyclerViewAdapter

class HairListActivity : AppCompatActivity() {

    private var listItemList: List<RecyclerListItem>? = null
    private var topRecyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nail_list)
        initRecyclerView()
        getListItemListData()
        initAdapter()
    }

    private fun initRecyclerView() {
        topRecyclerView = findViewById(R.id.recycler_view)
        val llm = LinearLayoutManager(this)
        topRecyclerView!!.layoutManager = llm
        topRecyclerView!!.setHasFixedSize(true)
    }

    private fun getListItemListData() {
        listItemList = MainActivityDataHolder.hairItemList
    }

    private fun initAdapter() {
        val nailClickListener =  { listItem: RecyclerListItem ->
            val intent = Intent(this@HairListActivity, ItemDescriptionActivity::class.java)
            intent.putExtra(MainActivityExtras.ITEM_NAME.toString(), listItem.name)
            intent.putExtra(MainActivityExtras.ITEM_DESCRIPTION.toString(), listItem.description)
            intent.putExtra(MainActivityExtras.ITEM_IMAGE_ID.toString(), listItem.imageResourceId)
            startActivity(intent)
        }
        val topAdapter = TopRecyclerViewAdapter(this!!.listItemList!!, nailClickListener)
        topRecyclerView!!.adapter = topAdapter
    }
}
