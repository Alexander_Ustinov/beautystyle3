package com.homework.lab6.beautystyle3.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.homework.lab6.beautystyle3.R

class ItemDescriptionActivity : AppCompatActivity() {

    private var nailName: TextView? = null
    private var nailDescription: TextView? = null
    private var nailPhoto: ImageView? = null

    private var nailNameText: String? = null
    private var nailDescriptionText: String? = null
    private var nailPhotoId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nail)
        loadIntentData()
        initNailTextNameView(nailNameText)
        initNailImageView(nailPhotoId)
        initNailTextView(nailDescriptionText)
        initLayout()
    }

    private fun loadIntentData() {
        val loadedIntent = intent
        nailNameText = loadedIntent.getStringExtra(MainActivityExtras.ITEM_NAME.toString())
        nailDescriptionText = loadedIntent.getStringExtra(MainActivityExtras.ITEM_DESCRIPTION.toString())
        nailPhotoId = loadedIntent.getIntExtra(MainActivityExtras.ITEM_IMAGE_ID.toString(), 0)
    }

    private fun initNailTextNameView(name: String?) {
        nailName = TextView(this)
        nailName!!.text = name
    }

    private fun initNailImageView(imageId: Int) {
        nailPhoto = ImageView(this)
        nailPhoto!!.setImageResource(imageId)
    }

    private fun initNailTextView(description: String?) {
        nailDescription = TextView(this)
        nailDescription!!.text = description
    }

    private fun initLayout() {
        val nailDescriptionLayout = findViewById<LinearLayout>(R.id.linear_layout)
        val nailLayoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        nailDescriptionLayout.addView(nailPhoto, nailLayoutParams)
        nailDescriptionLayout.addView(nailName, nailLayoutParams)
        nailDescriptionLayout.addView(nailDescription, nailLayoutParams)
    }
}
