package com.homework.lab6.beautystyle3.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.homework.lab6.beautystyle3.MainActivityDataHolder
import com.homework.lab6.beautystyle3.R
import com.homework.lab6.beautystyle3.RecyclerListItem
import com.homework.lab6.beautystyle3.adapter.TopRecyclerViewAdapter


class NailListActivity : AppCompatActivity() {

    private var listItemList: List<RecyclerListItem>? = null
    private var topRecyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nail_list)
        initRecyclerView()
        getItemListData()
        initAdapter()
    }

    private fun initRecyclerView() {
        topRecyclerView = findViewById(R.id.recycler_view)
        val llm = LinearLayoutManager(this)
        topRecyclerView!!.layoutManager = llm
        topRecyclerView!!.setHasFixedSize(true)
    }

    private fun getItemListData() {
        listItemList = MainActivityDataHolder.nailItemList
    }

    private fun initAdapter() {
        val nailClickListener =  { listItem: RecyclerListItem ->
            val intent = Intent(this@NailListActivity, ItemDescriptionActivity::class.java)
            intent.putExtra(MainActivityExtras.ITEM_NAME.toString(), listItem.name)
            intent.putExtra(MainActivityExtras.ITEM_DESCRIPTION.toString(), listItem.description)
            intent.putExtra(MainActivityExtras.ITEM_IMAGE_ID.toString(), listItem.imageResourceId)
            startActivity(intent)
        }
        val topAdapter = TopRecyclerViewAdapter(this!!.listItemList!!, nailClickListener)
        topRecyclerView!!.adapter = topAdapter
    }
}
