package com.homework.lab6.beautystyle3.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import com.homework.lab6.beautystyle3.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top)
        initAllViews()
    }

    private fun initAllViews() {
        initNailViews()
        initHairViews()
        initAddressViews()
    }

    private fun initNailViews() {
        val nailViews = findViewById<LinearLayout>(R.id.nail_views)
        nailViews.setOnClickListener(createNailClickListener())
    }

    private fun createNailClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this@MainActivity, NailListActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initHairViews() {
        val hairViews = findViewById<LinearLayout>(R.id.linear_layout)
        hairViews.setOnClickListener(createHairClickListener())
    }

    private fun createHairClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this@MainActivity, NailListActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initAddressViews() {
        val addressViews = findViewById<LinearLayout>(R.id.address_views)
        addressViews.setOnClickListener(createAddressClickListener())
    }

    private fun createAddressClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this@MainActivity, MapsActivity::class.java)
            startActivity(intent)
        }
    }
}