package com.homework.lab6.beautystyle3

class MainActivityDataHolder {
    companion object {
        val nailItemList: List<RecyclerListItem> = listOf(
                RecyclerListItem("Classic manicure", "This is classic manicure", R.drawable.emma),
                RecyclerListItem("Classic pedicure", "This is classic pedicure", R.drawable.emma),
                RecyclerListItem("Hardware manicure", "This is hardware manicure", R.drawable.emma)
        )

        val hairItemList: List<RecyclerListItem> = listOf(
                RecyclerListItem("Hair1", "Hair1", R.drawable.emma),
                RecyclerListItem("Hair12", "Hair12", R.drawable.emma),
                RecyclerListItem("Hair123", "Hair123", R.drawable.emma)
        )
    }
}