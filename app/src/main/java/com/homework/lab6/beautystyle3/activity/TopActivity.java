package com.homework.lab6.beautystyle3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.homework.lab6.beautystyle3.R;

public class TopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);
        initAllViews();
    }

    private void initAllViews() {
        initNailViews();
        initHairViews();
        initAddressViews();
    }

    private void initNailViews() {
        LinearLayout nailViews = findViewById(R.id.nail_views);
        nailViews.setOnClickListener(createNailClickListener());
    }

    private View.OnClickListener createNailClickListener() {
        return v -> {
            Intent intent = new Intent(TopActivity.this, NailListActivity.class);
            startActivity(intent);
        };
    }

    private void initHairViews() {
        LinearLayout hairViews = findViewById(R.id.hair_views);
        hairViews.setOnClickListener(createHairClickListener());
    }

    private View.OnClickListener createHairClickListener() {
        return v -> {
            Intent intent = new Intent(TopActivity.this, HairListActivity.class);
            startActivity(intent);
        };
    }

    private void initAddressViews() {
        LinearLayout addressViews = findViewById(R.id.address_views);
        addressViews.setOnClickListener(createAddressClickListener());
    }

    private View.OnClickListener createAddressClickListener() {
        return v -> {
            Intent intent = new Intent(TopActivity.this, MapsActivity.class);
            startActivity(intent);
        };
    }
}
