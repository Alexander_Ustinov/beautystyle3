package com.homework.lab6.beautystyle3;

import java.util.ArrayList;
import java.util.List;

public class TopActivityDataHolder {

    public final static List<ListItem> nailItemList = new ArrayList<ListItem>() {{
        add(new RecyclerListItem() {{
                setName("Classic manicure");
                setDescription("This is classic manicure");
                setImageResourceId(R.drawable.emma);
            }});
        add(new RecyclerListItem() {{
            setName("Classic pedicure");
            setDescription("This is classic pedicure");
            setImageResourceId(R.drawable.emma);
        }});
        add(new RecyclerListItem() {{
            setName("Hardware manicure");
            setDescription("This is hardware manicure");
            setImageResourceId(R.drawable.emma);
        }});
    }};

    public final static List<ListItem> hairItemList = new ArrayList<ListItem>() {{
        add(new RecyclerListItem() {{
            setName("Hair1");
            setDescription("Hair1 description");
            setImageResourceId(R.drawable.emma);
        }});
        add(new RecyclerListItem() {{
            setName("Hair2");
            setDescription("Hair2 description");
            setImageResourceId(R.drawable.emma);
        }});
        add(new RecyclerListItem() {{
            setName("Hair3");
            setDescription("Hair3 description");
            setImageResourceId(R.drawable.emma);
        }});
    }};
}
