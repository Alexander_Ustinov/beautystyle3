package com.homework.lab6.beautystyle3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.homework.lab6.beautystyle3.R;

public class ItemDescriptionActivity extends AppCompatActivity {

    private TextView nailName;
    private TextView nailDescription;
    private ImageView nailPhoto;

    private String nailNameText;
    private String nailDescriptionText;
    private int nailPhotoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nail);
        loadIntentData();
        initNailTextNameView(nailNameText);
        initNailImageView(nailPhotoId);
        initNailTextView(nailDescriptionText);
        initLayout();
    }

    private void loadIntentData() {
        Intent loadedIntent = getIntent();
        nailNameText = loadedIntent.getStringExtra(TopActivityExtras.ITEM_NAME.toString());
        nailDescriptionText = loadedIntent.getStringExtra(TopActivityExtras.ITEM_DESCRIPTION.toString());
        nailPhotoId = loadedIntent.getIntExtra(TopActivityExtras.ITEM_IMAGE_ID.toString(), 0);
    }

    private void initNailTextNameView(String name) {
        nailName = new TextView(this);
        nailName.setText(name);
    }

    private void initNailImageView(int imageId) {
        nailPhoto = new ImageView(this);
        nailPhoto.setImageResource(imageId);
    }

    private void initNailTextView(String description) {
        nailDescription = new TextView(this);
        nailDescription.setText(description);
    }

    private void initLayout() {
        LinearLayout nailDescriptionLayout = findViewById(R.id.linear_layout);
        LinearLayout.LayoutParams nailLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        nailDescriptionLayout.addView(nailPhoto, nailLayoutParams);
        nailDescriptionLayout.addView(nailName, nailLayoutParams);
        nailDescriptionLayout.addView(nailDescription, nailLayoutParams);
    }
}
