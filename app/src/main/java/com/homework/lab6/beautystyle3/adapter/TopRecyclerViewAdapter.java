package com.homework.lab6.beautystyle3.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.homework.lab6.beautystyle3.ListItem;
import com.homework.lab6.beautystyle3.R;

import java.util.List;

public class TopRecyclerViewAdapter extends RecyclerView.Adapter<TopRecyclerViewAdapter.NailViewHolder> {

    class NailViewHolder extends RecyclerView.ViewHolder {
        CardView nailCardView;
        TextView nailName;
        TextView nailDescription;
        ImageView nailPhoto;

        NailViewHolder(View itemView) {
            super(itemView);
            nailCardView = itemView.findViewById(R.id.cv);
            nailName = itemView.findViewById(R.id.nail_name);
            nailDescription = itemView.findViewById(R.id.nail_description);
            nailPhoto = itemView.findViewById(R.id.nail_photo);
            itemView.setOnClickListener(v -> {
                ListItem listItem = listItems.get(getLayoutPosition());
                onNailClickListener.onNailClick(listItem);
            });
        }
    }

    public interface OnNailClickListener {
        void onNailClick(ListItem listItem);
    }

    private OnNailClickListener onNailClickListener;
    private List<ListItem> listItems;

    public TopRecyclerViewAdapter(List<ListItem> listItems, OnNailClickListener onNailClickListener) {
        this.listItems = listItems;
        this.onNailClickListener = onNailClickListener;
    }

    @NonNull
    @Override
    public TopRecyclerViewAdapter.NailViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        View nailView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new NailViewHolder(nailView);
    }

    @Override
    public void onBindViewHolder(@NonNull TopRecyclerViewAdapter.NailViewHolder holder, int position) {
        holder.nailName.setText(listItems.get(position).getName());
        holder.nailDescription.setText(listItems.get(position).getDescription());
        holder.nailPhoto.setImageResource(listItems.get(position).getImageResourceId());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }
}
