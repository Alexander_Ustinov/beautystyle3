package com.homework.lab6.beautystyle3;

public interface ListItem {
    String getName();
    void setName(String name);
    String getDescription();
    int getImageResourceId();
}
