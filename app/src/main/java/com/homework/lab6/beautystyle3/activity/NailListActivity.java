package com.homework.lab6.beautystyle3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.homework.lab6.beautystyle3.ListItem;
import com.homework.lab6.beautystyle3.R;
import com.homework.lab6.beautystyle3.TopActivityDataHolder;
import com.homework.lab6.beautystyle3.adapter.TopRecyclerViewAdapter;

import java.util.List;

public class NailListActivity extends AppCompatActivity {

    private List<ListItem> nailItemList;
    private RecyclerView topRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nail_list);
        initRecyclerView();
        initData();
        initAdapter();
    }

    private void initRecyclerView() {
        topRecyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        topRecyclerView.setLayoutManager(llm);
        topRecyclerView.setHasFixedSize(true);
    }

    private void initData() {
        nailItemList = TopActivityDataHolder.nailItemList;
    }

    private void initAdapter() {

        TopRecyclerViewAdapter.OnNailClickListener nailClickListener = nailListItem -> {
            Intent intent = new Intent(NailListActivity.this, ItemDescriptionActivity.class);
            intent.putExtra(TopActivityExtras.ITEM_NAME.toString(), nailListItem.getName());
            intent.putExtra(TopActivityExtras.ITEM_DESCRIPTION.toString(), nailListItem.getDescription());
            intent.putExtra(TopActivityExtras.ITEM_IMAGE_ID.toString(), nailListItem.getImageResourceId());
            startActivity(intent);
        };

        TopRecyclerViewAdapter topAdapter = new TopRecyclerViewAdapter(nailItemList, nailClickListener);
        topRecyclerView.setAdapter(topAdapter);
    }
}
