package com.homework.lab6.beautystyle3.activity;

public enum TopActivityExtras {
    ITEM_NAME("nailName"),
    ITEM_DESCRIPTION("nailDescription"),
    ITEM_IMAGE_ID("nailImageId");

    private final String text;

    TopActivityExtras(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
