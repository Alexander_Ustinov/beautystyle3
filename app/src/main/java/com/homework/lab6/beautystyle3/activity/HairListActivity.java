package com.homework.lab6.beautystyle3.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.homework.lab6.beautystyle3.ListItem;
import com.homework.lab6.beautystyle3.R;
import com.homework.lab6.beautystyle3.RecyclerListItem;
import com.homework.lab6.beautystyle3.TopActivityDataHolder;
import com.homework.lab6.beautystyle3.adapter.TopRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class HairListActivity extends AppCompatActivity {

    private List<ListItem> hairItemList;
    private RecyclerView topRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair_list);
        initRecyclerView();
        initData();
        initAdapter();
    }

    private void initRecyclerView() {
        topRecyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        topRecyclerView.setLayoutManager(llm);
        topRecyclerView.setHasFixedSize(true);
    }

    private void initData() {
        hairItemList = TopActivityDataHolder.hairItemList;
    }

    private void initAdapter() {

        TopRecyclerViewAdapter.OnNailClickListener nailClickListener = nailListItem -> {
            Intent intent = new Intent(HairListActivity.this, ItemDescriptionActivity.class);
            intent.putExtra(TopActivityExtras.ITEM_NAME.toString(), nailListItem.getName());
            intent.putExtra(TopActivityExtras.ITEM_DESCRIPTION.toString(), nailListItem.getDescription());
            intent.putExtra(TopActivityExtras.ITEM_IMAGE_ID.toString(), nailListItem.getImageResourceId());
            startActivity(intent);
        };

        TopRecyclerViewAdapter topAdapter = new TopRecyclerViewAdapter(hairItemList, nailClickListener);
        topRecyclerView.setAdapter(topAdapter);
    }
}
